public class Prueba {
    public static void main(String[] args) {
    
        Nodo v3 = new Valor(3);
        Valor v4 = new Valor(4);
        Valor v5 = new Valor(5);

        Nodo m = new Operador('*', v4, v5);
        Operador s = new Operador('+', v3, m);
        
        System.out.println(s.valuar());
            
        if (v3 instanceof Valor) {
            Valor x = (Valor)v3;
            System.out.println(x.toString());
        }
        
        System.out.println(v3.getClass() == v4.getClass());
        
    }

    
}

