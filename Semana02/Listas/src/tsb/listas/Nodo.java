package tsb.listas;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class Nodo {

    Comparable info;
    Nodo next;

    public Nodo(Comparable info) {
        this.info = info;
        this.next = null;
    }
}
