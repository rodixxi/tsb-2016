import tsb.Lista;

import java.util.Iterator;


public class Principal {
    public static void main(String[] args) {
        long antes, despues;
        Lista<Integer> l = new Lista<>();
        for (int i = 0; i < 1000; i++) {
            l.insertar((int) (Math.random() * 1000));
        }


        antes = System.currentTimeMillis();
        int suma = sumarLista(l);
        despues = System.currentTimeMillis();
        System.out.println("Tiempo transcurrido: " + (despues - antes));
        System.out.println("La suma es: " + suma);

        antes = System.currentTimeMillis();
        suma = sumarListaIterador(l);
        despues = System.currentTimeMillis();
        System.out.println("Tiempo transcurrido: " + (despues - antes));
        System.out.println("La suma es: " + suma);
    }

    private static int sumarListaIterador(Lista<Integer> l) {
        int suma = 0;

        for (Integer x: l)
            suma += x;

        Iterator i = l.iterator();
        while(i.hasNext()) {
            Object x = i.next();
            suma += (int)x;
        }
        return suma;
    }

    private static int sumarLista(Lista<Integer> l) {
        int suma = 0;
        for (int i = 0; i < l.size(); i++) {
            suma += l.get(i);
        }
        return suma;
    }
}
